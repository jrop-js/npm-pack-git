# 📦 npm-pack-git 📦

> `npm pack` a specific Git commit

Streamline your deployment process! 🎊

## Installation 📐

```sh
npm install --save-dev npm-pack-git
```

## Use 🏗

Modify your deployment process: instead of using `npm pack` to produce your build-artifact, modify your package.json:

```json
{
  "scripts": {
    "pack:master": "npm-pack-git master"
  }
}
```

Now run `npm run pack:master` and the script will produce a packed version of your master branch.

## CLI 💻

```txt
Usage
  npm-pack-git [options] [<tree-ish>]

Options
  <tree-ish>    The commit/branch/tag to export
                default: 'master'
  --clean       Clean the temp directory before packing
  --cwd/-d      Set the directory to run in
  --help/-h     Show help
  --tmp/-t      Set temp directory
                default: ./.pack/
  --version/-v  Show version
```

## Licence (ISC) 🗄

ISC License (ISC)
Copyright 2017 <jrapodaca@gmail.com>

Permission to use, copy, modify, and/or distribute this software for any purpose with or without fee is hereby granted, provided that the above copyright notice and this permission notice appear in all copies.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
