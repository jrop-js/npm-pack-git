const {name} = require('../package')
export default `
Usage
  ${name} [options] [<tree-ish>]

Options
  <tree-ish>      The commit/branch/tag to export
                  default: 'master'
  --clean         Clean the temp directory before packing
  --cwd/-d        Set the directory to run in
  --help/-h       Show help
  --npm-ci        Install using \`npm ci\`
  --tmp/-t        Set temp directory
                  default: ./.pack/
  --version/-v    Show version
  --yarn          Use yarn to install/pack
  --yarn-install  Use yarn to install
  --yarn-pack     Use yarn to pack
`.trim()
