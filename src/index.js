import minimist from 'minimist'

const {version} = require('../package')

const args = minimist(process.argv.slice(2), {
	alias: {
		d: 'cwd',
		h: 'help',
		t: 'tmp',
		v: 'version',
	},
})
if (args.version) {
	console.log(version)
} else if (args.help) {
	console.log(require('./help').default)
	process.exitCode = 1
} else {
	require('./main').default(args)
}
