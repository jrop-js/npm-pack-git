import assert from 'assert'
import del from 'del'
import event from 'event-to-promise'
import execa from 'execa'
import fs from 'fs'
import mkdirp from 'mkdirp'
import path from 'path'
import tar from 'tar'
import thunk from 'thunk-to-promise'
import _ from 'lodash'

const which = bin =>
	execa('which', [bin])
		.then(c => c.stdout)
		.catch(e => null)

async function lastLine(cmd, procArgs, opts) {
	const proc = execa(cmd, procArgs, opts)
	proc.stdout.on('data', d => process.stdout.write(d))
	proc.stderr.on('data', d => process.stdout.write(d))
	const {stdout} = await proc
	return _.last(stdout.split('\n'))
}

const npmPack = args => lastLine('npm', ['pack'], {cwd: args.tmp})
async function yarnPack(args) {
	const tarMsg = JSON.parse(
		await lastLine('yarn', ['pack', '--json'], {cwd: args.tmp})
	).data
	return path.basename(/"([^"]*)"\.$/.exec(tarMsg)[1])
}

async function main(args) {
	args = _.merge(
		{
			cwd: process.cwd(),
		},
		args
	)
	args = _.merge(
		{
			tmp: path.resolve(args.cwd, '.pack'),
		},
		args
	)
	args.treeIsh = args._[0] || 'master'
	args.cwd = path.resolve(args.cwd)
	args.tmp = path.resolve(args.tmp)
	if (args.yarn) {
		args['yarn-install'] = true
		args['yarn-pack'] = true
	}
	assert(!(args['yarn-install'] && args['npm-ci']), '--yarn-install & --npm-ci are exclusive')

	if (!(await which('git'))) {
		console.error('error: git not found')
		process.exitCode = 1
	}
	if (!(await which('npm'))) {
		console.error('error: npm not found')
		process.exitCode = 1
	}

	// clear out temp-directory
	await del(args.clean ? args.tmp : ['**', '!node_modules'], {
		cwd: args.tmp,
		dot: true,
	})

	await thunk(done => mkdirp(args.tmp, done))

	const git = execa('git', ['archive', '--format', 'tar', args.treeIsh], {
		cwd: args.cwd,
	})
	const tarStream = git.stdout.pipe(
		tar.x({
			cwd: args.tmp,
		})
	)
	await event(tarStream, 'end')

	// install dependencies
	await execa(
		args['yarn-install'] ? 'yarn' : 'npm',
		[args['npm-ci'] ? 'ci' : 'install'],
		{
			cwd: args.tmp,
			stdio: 'inherit',
		}
	)
	const tgz = await (args['yarn-pack'] ? yarnPack(args) : npmPack(args))
	await thunk(done =>
		fs.copyFile(path.resolve(args.tmp, tgz), path.resolve(args.cwd, tgz), done)
	)
}
export default args =>
	main(args).catch(e => {
		process.exitCode = 1
		console.error(e)
	})
